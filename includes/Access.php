<?php

namespace RRZE\PrivateSite;

defined('ABSPATH') || exit;

use RRZE\PrivateSite\Crawler\Siteimprove;

class Access
{
    /**
     * Try To Access
     * @param boolean $rest
     * @return boolean
     */
    public static function try($rest = false)
    {
        if (is_super_admin()) {
            return true;
        }

        // Checks if the user is a registered member of the website.
        if (permissions()->isUserMember()) {
            return true;
        }

        // Check password if available.
        if (permissions()->checkPassword()) {
            return true;
        }

        // Checks whether the IP address of the user is within the permitted IP range.
        if (permissions()->checkIpAddressRange(get_option(PRIVATE_SITE_ALLOW_IPADDR_OPTION))) {
            return true;
        }

        // Checks SSO access
        if (get_option(PRIVATE_SITE_SSO_OPTION) && permissions()->checkSSO()) {
            return true;
        }

        if (is_feed() && permissions()->hasFeedToken()) {
            return true;
        }

        if (get_option(PRIVATE_SITE_SITEIMPROVE_OPTION)) {
            $ipAddresses = apply_filters('rrze_ac_siteimprove_crawler_ip_addresses', Siteimprove::getIpAddresses());
            if (permissions()->checkIpAddressRange($ipAddresses)) {
                return true;
            }
        }

        if ($rest) {
            return false;
        }

        do_action(
            'rrze.log.notice',
            [
                'plugin' => 'rrze-private-site',
                'method' => __METHOD__,
                'message' => 'Access denied',
            ]
        );

        if (is_feed()) {
            self::feedDeniedMessage();
        } else {
            self::deniedMessage();
        }
    }

    /**
     * Denied Message
     */
    public static function deniedMessage()
    {
        $message = '';
        $currentUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']
            === 'on' ? "https" : "http") .
            "://" . $_SERVER['HTTP_HOST'] .
            ($_SERVER['REQUEST_URI'] ?? '');

        $loginUrl = wp_login_url($currentUrl);

        $message .= '<h3>' . __('Access denied', 'rrze-private-site') . '</h3>';
        $message .= '<p>' . __('You tried to access a resource on this website, but you do not have sufficient rights to do so.', 'rrze-private-site');

        if (permissions()->simplesamlAuth && permissions()->simplesamlAuth->isAuthenticated()) {
            $simplesamlLoginOut = permissions()->simplesamlAuth->getLogoutURL($currentUrl);
            $message .= sprintf(
                ' <a href="%1$s">%2$s</a>.',
                $simplesamlLoginOut,
                __('Log out through the single sign-on service', 'rrze-private-site')
            );
            // Save the current session and clean any left overs that could interfere 
            // with the normal application behaviour.           
            \SimpleSAML\Session::getSessionFromRequest()->cleanup();
        }

        if (permissions()->simplesamlAuth && !permissions()->simplesamlAuth->isAuthenticated()) {
            // Save the current session and clean any left overs that could interfere 
            // with the normal application behaviour.
            \SimpleSAML\Session::getSessionFromRequest()->cleanup();

            // RRZE Legal Plugin
            include_once ABSPATH . 'wp-admin/includes/plugin.php';
            if (
                (is_plugin_active('rrze-legal/rrze-legal.php') ||
                    is_plugin_active_for_network('rrze-legal/rrze-legal.php')) &&
                method_exists('\RRZE\Legal\Consent\Cookies', 'setEssentialCookie') &&
                empty($_COOKIE['rrze-legal-consent'])
            ) {
                \RRZE\Legal\Consent\Cookies::setEssentialCookie();
            }

            $simplesamlLoginUrl = permissions()->simplesamlAuth->getLoginURL($currentUrl);
            $message .= '<p>' . sprintf(
                /* translators: %s is the Single Sign-On log in URL */
                __('If you have a central log in service account, <a href="%s">please log in through the single sign-on service</a>.', 'rrze-private-site'),
                $simplesamlLoginUrl
            ) . '</p>';
        }

        if (!permissions()->hasPasswordOption()) {
            $message .= '<p>' . __('If you have a password to access this resource, please enter it in the following field.', 'rrze-private-site') . PHP_EOL;
            $message .= '<form method="post">';
            $message .= wp_nonce_field('rrze_private_site_submit_password_wpnonce', '_wpnonce', true, false);
            $message .= '<input type="password" name="rrze_private_site_password" value="" style="padding: 0 8px; min-height: 23px;">' . PHP_EOL;
            $message .= '<input type="submit" name="rrze_private_site_submit_password" id="submit" class="button button-primary" value="' . __('Send password', 'rrze-private-site') . '"></p>';
            $message .= '</form>';
        }

        $message .= wpautop('<a href="' . $loginUrl . '">' . esc_html(__('If you are a member of this website please log in.', 'rrze-private-site')) . '</a>');

        $message .= self::getContact();

        wp_die(
            $message,
            __('Forbidden', 'rrze-private-site'),
            [
                'response' => '403',
                'back_link' => false
            ]
        );
    }

    /**
     * Feed Denied Message
     */
    protected static function feedDeniedMessage()
    {
        wp_die(
            __('No feed available.', 'rrze-private-site'),
            __('Forbidden', 'rrze-private-site'),
            [
                'response' => '403',
                'back_link' => false
            ]
        );
    }

    /**
     * Get Website Contact
     * @return string
     */
    protected static function getContact()
    {
        $output = '';
        $contact = [];

        if (!$siteAdminName = get_option(PRIVATE_SITE_CONTACT_ADMIN_NAME_OPTION)) {
            $blogId = get_current_blog_id();
            $admins = get_users([
                'role' => 'administrator',
                'blog_id' => $blogId
            ]);
            if (!empty($admins)) {
                foreach ($admins as $user) {
                    $contact[] = sprintf(
                        '<a href="mailto:%1$s">%2$s</a>',
                        Utils::encodeEmail($user->data->user_email),
                        $user->data->display_name
                    );
                }
            }
        } else {
            $siteAdminEmail = get_option('admin_email', '');
            $contact[] = sprintf(
                '<a href="mailto:%1$s">%2$s</a>',
                Utils::encodeEmail($siteAdminEmail),
                $siteAdminName
            );
        }

        if ($contact) {
            $output .= '<p>' . __('Contact:', 'rrze-private-site') . '<br>';
            $output .= implode('<br>', $contact) . '</p>';
        }

        return $output;
    }
}
