<?php

namespace RRZE\PrivateSite;

defined('ABSPATH') || exit;

use RRZE\PrivateSite\Settings;
use RRZE\PrivateSite\Media\{Files, Rewrite};

class Main
{
    /**
     * [protected description]
     * @var string
     */
    protected $ssoPlugin = 'rrze-sso/rrze-sso.php';

    /**
     * [protected description]
     * @var string
     */
    protected $ssoPluginOptionName = 'rrze_sso';

    /**
     * [protected description]
     * @var null|object
     */
    protected $simplesamlAuth = null;

    /**
     * [protected description]
     * @var null|array
     */
    protected $personAttributes = null;

    /**
     * [protected description]
     * @var null|array
     */
    protected $personAffiliation = null;

    /**
     * [protected description]
     * @var null|array
     */
    protected $personEntitlement = null;

    /**
     * Used in the template_redirect hook if the rrze-ac plugin is active.
     * @var boolean
     */
    protected $rrzeACPluginAccessAllowed = false;

    /**
     * [__construct description]
     */
    public function __construct()
    {
        new Settings;

        add_filter('rrzecache_skip_cache', '__return_true');

        add_filter('rrze_ac_access_allowed', function ($accesAllowed) {
            if ($accesAllowed) {
                $this->rrzeACPluginAccessAllowed = true;
            }
            return $accesAllowed;
        });

        if (get_option(PRIVATE_SITE_OPTION)) {
            $this->init();
        } else {
            Rewrite::htaccessSetup(true);
            delete_transient('private_site_check_rewrite');
        }
    }

    private function init()
    {
        // RRZE SSO Plugin: User Registration Disabled
        add_filter('rrze_sso_registration', '__return_false');
        // RRZE Cache Plugin: Flush Cache
        do_action('rrzecache_flush_cache');

        add_filter('privacy_on_link_title', [$this, 'privacyOnLinkTitle']);
        add_filter('privacy_on_link_text', [$this, 'privacyOnLinkText']);

        add_action('template_redirect', [$this, 'templateRedirect'], 1);

        // Discourage search engines from indexing this website
        update_option('blog_public', '0');

        // Check rewrite rules
        if (!get_transient('private_site_check_rewrite')) {
            set_transient('private_site_check_rewrite', 1, DAY_IN_SECONDS);
            Rewrite::htaccessSetup();
            add_action('init', [$this, 'checkRewrite']);
        }
        // Load requested file
        add_action('init', [$this, 'requestFile'], 1);

        // WP-REST-API
        add_filter('rest_pre_dispatch', [$this, 'restAccess'], 1, 3);
    }

    /**
     * Check rewrite rules
     *
     * @return void
     */
    public function checkRewrite()
    {
        if (is_admin()) {
            global $pagenow;
            if (Rewrite::checkRewriteRules()) {
                wp_redirect(admin_url($pagenow ? $pagenow : ''));
                exit();
            } else {
                add_action('admin_notices', [$this, 'adminErrorcheckRewrite']);
            }
        }
    }

    public function requestFile()
    {
        if (!empty($_GET['private_site_file'])) {
            if (!empty($_GET['private_site_rewrite_test'])) {
                die('rewrite test passed');
            }
            Files::getFile($_GET['private_site_file']);
            exit();
        }
    }

    /**
     * Template Redirect
     */
    public function templateRedirect()
    {
        if (defined('DOING_CRON') && DOING_CRON) {
            return;
        }
        if (get_option(PRIVATE_SITE_OPTION) && !$this->rrzeACPluginAccessAllowed) {
            $this->tryAccess();
        }
    }

    /**
     * Set Privacy On Link Title
     * @param  string $title
     * @return string
     */
    public function privacyOnLinkTitle($title)
    {
        $title = __('Access restriction enabled', 'rrze-private-site');
        return $title;
    }

    /**
     * Set Privacy On Link Text
     * @param  string $text
     * @return string
     */
    public function privacyOnLinkText($text)
    {
        $text = __('Access restriction enabled', 'rrze-private-site');
        return $text;
    }

    /**
     * Try To Access
     * @param  boolean $rest
     * @return boolean
     */
    protected function tryAccess($rest = false)
    {
        return Access::try($rest);
    }

    public function adminErrorcheckRewrite()
    {
        if (!current_user_can('manage_options')) {
            return;
        }
        $message = __("The RRZE Private Site Plugin is not configured properly. The files and documents can not be protected.", 'rrze-private-site');
        $message .= '<br/>';
        if (is_network_admin() || is_super_admin()) {
            $message .= __("The following rewrite commands must be added in the .htaccess file after the WordPress line <code>RewriteRule ^index\\.php$ - [L]</code>.", 'rrze-private-site');
            $message .= '<p>' . implode('<br>', Rewrite::rewriteRules()) . '</p>';
        } else {
            $message .= __("Please contact your system administrator.", 'rrze-private-site');
        }
        printf('<div class="notice notice-warning"><p>%s</p></div>', $message);
    }

    public function restAccess($response, $server, $request)
    {
        if ($this->tryAccess(true) !== false) {
            return $response;
        }

        do_action(
            'rrze.log.notice',
            [
                'plugin' => 'rrze-private-site',
                'method' => __METHOD__,
                'message' => 'REST API Error: Access denied',
            ]
        );

        $response = new \WP_REST_Response(
            __('REST API support is restricted.', 'rrze-private-site'),
            403
        );

        return $response;
    }
}
