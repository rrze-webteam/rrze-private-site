<?php

namespace RRZE\PrivateSite\Media;

defined('ABSPATH') || exit;

use RRZE\PrivateSite\Access;

class Files
{
    public static function getFile($requestedFile)
    {
        $requestedFile = isset($requestedFile) ? $requestedFile : '';
        $uploadDir = wp_upload_dir();
        $baseDir = !empty($uploadDir['basedir']) ? $uploadDir['basedir'] : '';
        if (str_starts_with($requestedFile, 'wp-content/')) {
            $file = ABSPATH . trim(str_replace('..', '', $requestedFile), '/\\');
        } elseif (str_starts_with($requestedFile, 'files/')) {
            $file = trailingslashit($baseDir) . trim(str_replace('..', '', substr($requestedFile, strlen('files/'))), '/\\');
        } else {
            $file = trailingslashit($baseDir) . trim(str_replace('..', '', $requestedFile), '/\\');
        }

        if (!$file || !is_file($file)) {
            do_action(
                'rrze.log.error',
                'Plugin: {plugin} Error: {error}',
                [
                    'plugin' => 'rrze-private-site',
                    'error' => __('The requested file was not found.', 'rrze-private-site'),
                    'file' => $file,
                    'requested_file' => $requestedFile,
                    'upload_dir' => $uploadDir
                ]
            );
            wp_die(
                __('The requested file was not found.', 'rrze-private-site'),
                __('Not Found', 'rrze-private-site'),
                [
                    'response' => '404',
                    'back_link' => false
                ]
            );
        }

        $mime = wp_check_filetype($file);

        if (isset($mime['type']) && $mime['type']) {
            $mimetype = $mime['type'];
        } else {
            do_action(
                'rrze.log.error',
                'Plugin: {plugin} Error: {error}',
                [
                    'plugin' => 'rrze-private-site',
                    'error' => __('The request was due lack of client permission not performed.', 'rrze-private-site'),
                    'file' => $file,
                    'mime' => $mime
                ]
            );
            wp_die(
                __('The request was due lack of client permission not performed.', 'rrze-private-site'),
                __('Forbidden', 'rrze-private-site'),
                [
                    'response' => '403',
                    'back_link' => false
                ]
            );
        }

        if (!defined('DONOTCACHEPAGE')) {
            define('DONOTCACHEPAGE', 1);
        }

        if (!defined('DONOTCACHEOBJECT')) {
            define('DONOTCACHEOBJECT', 1);
        }

        if (!defined('DONOTMINIFY')) {
            define('DONOTMINIFY', 1);
        }

        Access::try();

        header('Content-Type: ' . $mimetype);
        header('Content-Length: ' . filesize($file));

        $last_modified = gmdate('D, d M Y H:i:s', filemtime($file));
        $etag = '"' . md5($last_modified) . '"';
        header("Last-Modified: $last_modified GMT");
        header('ETag: ' . $etag);
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Expires: Thu, 01 Dec 1994 16:00:00 GMT');

        $client_etag = isset($_SERVER['HTTP_IF_NONE_MATCH']) ? stripslashes($_SERVER['HTTP_IF_NONE_MATCH']) : false;

        if (!isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
            $_SERVER['HTTP_IF_MODIFIED_SINCE'] = false;
        }

        $client_last_modified = trim($_SERVER['HTTP_IF_MODIFIED_SINCE']);

        $client_modified_timestamp = $client_last_modified ? strtotime($client_last_modified) : 0;

        $modified_timestamp = strtotime($last_modified);

        if (($client_last_modified && $client_etag) ? (($client_modified_timestamp >= $modified_timestamp) && ($client_etag == $etag)) : (($client_modified_timestamp >= $modified_timestamp) || ($client_etag == $etag))) {
            status_header(304);  // Not Modified
            exit();
        }

        if (ob_get_length()) {
            ob_clean();
        }

        flush();

        readfile($file);
        exit();
    }
}
