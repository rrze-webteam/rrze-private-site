<?php

namespace RRZE\PrivateSite\Media;

defined('ABSPATH') || exit;

class Rewrite
{
    public static function checkRewriteRules()
    {
        $uploadDir = wp_upload_dir();
        $protectedFileTest = 'index.php?private_site_rewrite_test=1';
        $checkUrl = $uploadDir['baseurl'] . '/' . $protectedFileTest;

        $check = wp_remote_get($checkUrl, ['sslverify' => false, 'httpversion' => '1.1']);
        if (is_wp_error($check) || !isset($check['response']['code']) || 200 != $check['response']['code'] || !isset($check['body']) || 'rewrite test passed' != $check['body']) {
            return false;
        }

        return true;
    }

    public static function rewriteRules()
    {
        $rewriteRules[] = '# BEGIN RRZE PRIVATE SITE WP PLUGIN';
        if (is_multisite()) {
            $hostCondition = self::getHostCondition();
            $rewriteRules[] = '<code>' . $hostCondition . ' [NC]</code>';
        }

        $rewriteRules[] = '<code>RewriteRule ' . self::getRewriteRules() . '</code>';
        $rewriteRules[] = '# END RRZE PRIVATE SITE WP PLUGIN';
        return $rewriteRules;
    }

    public static function getHostCondition()
    {
        $httpHost = str_replace('.', '\.', untrailingslashit(preg_replace("(^https?://)", "", get_site_url())));
        if (is_multisite() && !is_subdomain_install()) {
            $ary = explode('/', $httpHost);
            $path = $ary[array_key_last($ary)];
            return 'RewriteCond %{REQUEST_URI} ^\/' . $path . '\/';
        } else {
            return 'RewriteCond %{HTTP_HOST} ^' . $httpHost;
        }
    }

    public static function getRewriteRules()
    {
        if (is_multisite() && is_subdomain_install()) {
            return '^(files/.+|wp-content/blogs\.dir/([0-9]+)/files/.+|wp-content/uploads/sites/([0-9]+)/.+) index.php?private_site_file=$1 [QSA,L]';
        } elseif (is_multisite() && !is_subdomain_install()) {
            return '^([_0-9a-zA-Z-]+)/(files/.+|wp-content/blogs\.dir/([0-9]+)/files/.+|wp-content/uploads/sites/([0-9]+)/.+) index.php?private_site_file=$2 [QSA,L]';
        } else {
            return '^wp-content/uploads/.+ index.php?private_site_file=$1 [QSA,L]';
        }
    }

    public static function htaccessSetup($delete = false)
    {
        include_once ABSPATH . 'wp-admin/includes/file.php';
        include_once ABSPATH . 'wp-admin/includes/misc.php';
        $existingRules = [];
        $htaccessFile = get_home_path() . '.htaccess';

        if (
            file_exists($htaccessFile)
            && is_writable($htaccessFile)
            && self::existMarker($htaccessFile, 'RRZE PRIVATE SITE WP PLUGIN')
        ) {
            $existingRules = array_filter(extract_from_markers($htaccessFile, 'RRZE PRIVATE SITE WP PLUGIN'));
            $rewriteConditions = [];
            foreach ($existingRules as $rule) {
                if (
                    strpos($rule, 'RewriteCond %{HTTP_HOST} ') !== false
                    || strpos($rule, 'RewriteCond %{REQUEST_URI} ') !== false
                ) {
                    $line = preg_replace('/\s+/', ' ', trim($rule));
                    $line = rtrim(rtrim($line, ' [NC,OR]'), ' [NC]');
                    $rewriteConditions[] = $line;
                }
            }

            $currentHostCondition = self::getHostCondition();
            if (!in_array($currentHostCondition, $rewriteConditions)) {
                $rewriteConditions[] = $currentHostCondition;
            }

            if ($delete) {
                $key = array_search($currentHostCondition, $rewriteConditions);
                unset($rewriteConditions[$key]);
            }

            $newRules = [];
            foreach ($rewriteConditions as $key => $rule) {
                if (array_key_last($rewriteConditions) != $key) {
                    $newRules[] = $rule . ' [NC,OR]';
                } else {
                    $newRules[] = $rule . ' [NC]';
                }
            }
            if (count($newRules)) {
                if (!is_multisite()) {
                    $newRules = [];
                }
                $newRules[] = 'RewriteRule ' . self::getRewriteRules();
            }

            return insert_with_markers($htaccessFile, 'RRZE PRIVATE SITE WP PLUGIN', $newRules);
        } else {
            return false;
        }
    }

    private static function existMarker($filename, $marker)
    {
        if (!file_exists($filename)) {
            return false;
        }

        $begin = false;
        $end = false;
        $markerdata = explode("\n", implode('', file($filename)));
        foreach ($markerdata as $markerline) {
            if (false !== strpos($markerline, '# BEGIN ' . $marker)) {
                $begin = true;
            }
            if (false !== strpos($markerline, '# END ' . $marker)) {
                $end = true;
            }
        }

        return $begin && $end;
    }
}
