<?php

namespace RRZE\PrivateSite;

defined('ABSPATH') || exit;

use RRZE\PrivateSite\Network\{IP, RemoteAddress};
use RRZE\PrivateSite\SSO\SimpleSAML;
use RRZE\PrivateSite\Crawler\Siteimprove;

class Permissions
{
    const SSO_PLUGIN = 'rrze-sso/rrze-sso.php';

    const SSO_PLUGIN_OPTION_NAME = 'rrze_sso';

    /**
     * public $simplesamlAuth
     * @var null|object
     */
    public $simplesamlAuth = null;

    /**
     * protected $personAttributes
     * @var null|array
     */
    protected $personAttributes = null;

    /**
     * protected $personAffiliation
     * @var null|array
     */
    protected $personAffiliation = null;

    /**
     * protected $personEntitlement
     * @var null|array
     */
    protected $personEntitlement = null;

    public function loaded()
    {
        // WP-REST-API
        add_filter(
            'rest_authentication_errors',
            fn($result) => empty($result) && !is_user_logged_in()
                ? new \WP_Error(
                    'rest_cannot_access',
                    __('Unauthorized access to the REST API.', 'rrze-private-site'),
                    ['status' => rest_authorization_required_code()]
                )
                : $result
        );
    }

    /**
     * Is User A Member Of The Website?
     * @return boolean
     */
    public function isUserMember()
    {
        if (!is_user_logged_in()) {
            return false;
        }

        return array_key_exists(
            get_current_blog_id(),
            get_blogs_of_user(get_current_user_id())
        );
    }

    /**
     * Check Password
     * @return boolean
     */
    public function checkPassword()
    {
        if (!$this->hasPasswordOption()) {
            return false;
        }

        if (isset($_POST['_wpnonce']) && wp_verify_nonce($_POST['_wpnonce'], 'rrze_private_site_submit_password_wpnonce')) {
            $password = isset($_POST['rrze_private_site_password']) ? sanitize_text_field($_POST['rrze_private_site_password']) : '';
            if (preg_match('/^[a-z0-9]{8,32}$/i', $password) && $password == get_option(PRIVATE_SITE_PASSWORD_OPTION)) {
                setcookie('rrze_private_site_password', Utils::crypt($password), strtotime('+1 day'), COOKIEPATH, COOKIE_DOMAIN, true);
                $location = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : site_url();
                wp_safe_redirect($location);
                exit;
            }
            return false;
        }

        if ($this->hasPasswordCookie()) {
            $password = Utils::crypt($_COOKIE['rrze_private_site_password'], 'decrypt');
            if (preg_match('/^[a-z0-9]{8,32}$/i', $password) && $password == get_option(PRIVATE_SITE_PASSWORD_OPTION)) {
                return true;
            }
        }

        unset($_COOKIE['rrze_private_site_password']);
        return false;
    }

    public function hasPasswordOption()
    {
        return empty(get_option(PRIVATE_SITE_PASSWORD_OPTION));
    }

    public function hasPasswordCookie()
    {
        return isset($_COOKIE['rrze_private_site_password']);
    }

    /**
     * Check Ip Address Range
     * @param  array  $ipAddress
     * @return boolean
     */
    public function checkIpAddressRange($ipAddress = [])
    {
        if (empty($ipAddress)) {
            return false;
        }

        if (!is_array($ipAddress)) {
            do_action(
                'rrze.log.warning',
                [
                    'plugin' => 'rrze-private-site',
                    'method' => __METHOD__,
                    'message' => 'Wrong IP address range type. Must be an array.'
                ]
            );
            return false;
        }

        $remoteAddr = $this->getRemoteIpAddress();

        if (!$remoteAddr) {
            do_action(
                'rrze.log.warning',
                [
                    'plugin' => 'rrze-private-site',
                    'method' => __METHOD__,
                    'message' => 'Remote IP address is UNKNOWN.'
                ]
            );
            return false;
        }

        $ip = IP::fromStringIP($remoteAddr);

        if ($ip->isInRanges($ipAddress)) {
            return true;
        }

        do_action(
            'rrze.log.notice',
            [
                'plugin' => 'rrze-private-site',
                'method' => __METHOD__,
                'message' => sprintf('Remote IP address %s is not in range.', $remoteAddr)
            ]
        );
        return false;
    }

    public function getRemoteIpAddress()
    {
        $remoteAddress = new RemoteAddress();
        return $remoteAddress->getIpAddress();
    }

    /**
     * checkRemoteDomain
     * @param array $allowedDomains
     * @return boolean
     */
    public function checkRemoteDomain($allowedDomains)
    {
        if (empty($allowedDomains) || !is_array($allowedDomains)) {
            return true;
        }

        $remoteAddr = $this->getRemoteIpAddress();

        if (!$remoteAddr) {
            do_action(
                'rrze.log.warning',
                [
                    'plugin' => 'rrze-private-site',
                    'method' => __METHOD__,
                    'message' =>
                    'Remote IP address is UNKNOWN.'
                ]
            );
            return false;
        }

        $ip = IP::fromStringIP($remoteAddr);
        $hostname = $ip->getHostname();

        if ($hostname === null) {
            do_action(
                'rrze.log.notice',
                [
                    'plugin' => 'rrze-private-site',
                    'method' => __METHOD__,
                    'message' => sprintf('Cannot get hostname from remote IP address %s.', $remoteAddr)
                ]
            );
            return false;
        }

        foreach ($allowedDomains as $domain) {
            if (strrpos($domain, $hostname) !== false) {
                return true;
            }
        }

        do_action(
            'rrze.log.notice',
            [
                'plugin' => 'rrze-private-site',
                'method' => __METHOD__,
                'message' => sprintf('Remote hostname %s is not allowed.', $hostname)
            ]
        );
        return false;
    }

    /**
     * Check SSO
     * @return boolean
     */
    public function checkSSO()
    {
        if (!$this->checkSSOLoggedIn()) {
            return false;
        }

        if (!get_option(PRIVATE_SITE_ALLOW_AFFILIATION_OPTION) && !get_option(PRIVATE_SITE_ALLOW_ENTITLEMENT_OPTION)) {
            return true;
        }

        if ($this->personAffiliation && $this->checkPersonAffiliation(get_option(PRIVATE_SITE_ALLOW_AFFILIATION_OPTION))) {
            return true;
        }

        if ($this->personEntitlement && $this->checkPersonEntitlement(get_option(PRIVATE_SITE_ALLOW_ENTITLEMENT_OPTION))) {
            return true;
        }

        return false;
    }

    /**
     * Check if user is SSO logged in
     * @return boolean
     */
    public function checkSSOLoggedIn()
    {
        if (!$this->simplesamlAuth()) {
            return false;
        }

        if (!$this->simplesamlAuth->isAuthenticated()) {
            // Save the current session and clean any left overs that could interfere 
            // with the normal application behaviour.           
            \SimpleSAML\Session::getSessionFromRequest()->cleanup();
            return false;
        }

        $this->personAttributes = $this->simplesamlAuth->getAttributes();
        $this->personAffiliation = $this->personAttributes['eduPersonAffiliation'] ?? [];
        $this->personEntitlement = $this->personAttributes['eduPersonEntitlement'] ?? [];

        return true;
    }

    /**
     * SSO: Check if an instance of SimpleSAML can be initialized
     * @return boolean
     */
    public function simplesamlAuth()
    {
        if (Utils::isPluginActive(self::SSO_PLUGIN)) {
            if (is_multisite()) {
                $options = get_site_option(self::SSO_PLUGIN_OPTION_NAME);
            } else {
                $options = get_option(self::SSO_PLUGIN_OPTION_NAME);
            }
        } else {
            return false;
        }

        if (!isset($options['simplesaml_include']) || !isset($options['simplesaml_auth_source'])) {
            return false;
        }

        if (!file_exists(WP_CONTENT_DIR . $options['simplesaml_include'])) {
            return false;
        }

        $this->simplesamlAuth = new SimpleSAML($options);
        $this->simplesamlAuth = $this->simplesamlAuth->loaded();
        if ($this->simplesamlAuth === false) {
            return false;
        }

        return true;
    }

    /**
     * SSO: Check Person Affiliation
     * @param  array $affiliation
     * @return boolean
     */
    public function checkPersonAffiliation($affiliation)
    {
        if (empty($affiliation)) {
            return false;
        }

        if (!is_array($affiliation)) {
            do_action(
                'rrze.log.warning',
                [
                    'plugin' => 'rrze-private-site',
                    'method' => __METHOD__,
                    'message' => 'Wrong person affiliation attribute value. Must be an array.',
                    'person_atributes' => $this->personAttributes
                ]
            );
            return false;
        }

        foreach ($affiliation as $attribute) {
            if (in_array($attribute, $this->personAffiliation)) {
                return true;
            }
        }

        do_action(
            'rrze.log.warning',
            [
                'plugin' => 'rrze-private-site',
                'method' => __METHOD__,
                'message' => 'Wrong person affiliation attribute.',
                'person_atributes' => $this->personAttributes
            ]
        );
        return false;
    }

    /**
     * SSO: Check Person Entitlement
     * @param  array $entitlement
     * @return boolean
     */
    public function checkPersonEntitlement($entitlement)
    {
        if (empty($entitlement)) {
            return false;
        }

        if (!is_array($entitlement)) {
            do_action(
                'rrze.log.warning',
                [
                    'plugin' => 'rrze-private-site',
                    'method' => __METHOD__,
                    'message' => 'Wrong person entitlement attribute value. Must be an array.',
                    'person_atributes' => $this->personAttributes
                ]
            );
            return false;
        }

        foreach ($entitlement as $attribute) {
            if (in_array($attribute, $this->personEntitlement)) {
                return true;
            }
        }

        do_action(
            'rrze.log.warning',
            [
                'plugin' => 'rrze-private-site',
                'method' => __METHOD__,
                'message' => 'Wrong person entitlement attribute.',
                'person_atributes' => $this->personAttributes
            ]
        );
        return false;
    }

    /**
     * Check Siteimprove
     * @return boolean
     */
    public function checkSiteimprove()
    {
        $ipAddresses = Siteimprove::getIpAddresses();
        if (!empty($ipAddresses)) {
            if (!permissions()->checkIpAddressRange($ipAddresses)) {
                do_action(
                    'rrze.log.notice',
                    [
                        'plugin' => 'rrze-private-site',
                        'type' => __METHOD__,
                        'message' => 'Crawler IP address is not in range.'
                    ]
                );
                return false;
            }
        }
        return true;
    }

    /**
     * hasFeedToken
     * @return boolean
     */
    public function hasFeedToken()
    {
        $token = (string) get_option(PRIVATE_SITE_FEED_TOKEN_OPTION, '');
        $getToken = isset($_GET['token']) ? trim($_GET['token']) : '';

        if (!empty($token) && strtolower($token) === strtolower($getToken)) {
            return true;
        }

        return false;
    }
}
