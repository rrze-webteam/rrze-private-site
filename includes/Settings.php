<?php

namespace RRZE\PrivateSite;

defined('ABSPATH') || exit;

use RRZE\PrivateSite\Network\IPUtils;

class Settings
{
    /**
     * __construct
     */
    public function __construct()
    {
        add_action('admin_init', [$this, 'adminSettings']);
        add_filter('plugin_action_links_' . plugin()->getBasename(), [$this, 'pluginActionLink']);
    }

    /**
     * adminSettings
     */
    public function adminSettings()
    {
        register_setting('reading', PRIVATE_SITE_OPTION, [$this, 'validateActivation']);

        register_setting('reading', PRIVATE_SITE_PASSWORD_OPTION, [$this, 'validatePassword']);

        register_setting('reading', PRIVATE_SITE_ALLOW_IPADDR_OPTION, [$this, 'validateAllowIp']);

        register_setting('reading', PRIVATE_SITE_SSO_OPTION, [$this, 'validateSSO']);
        register_setting('reading', PRIVATE_SITE_ALLOW_AFFILIATION_OPTION, [$this, 'validateAllowAffiliation']);
        register_setting('reading', PRIVATE_SITE_ALLOW_ENTITLEMENT_OPTION, [$this, 'validateAllowEntitlement']);

        register_setting('reading', PRIVATE_SITE_FEED_TOKEN_OPTION, [$this, 'validateFeedToken']);

        register_setting('reading', PRIVATE_SITE_CONTACT_ADMIN_NAME_OPTION, 'sanitize_text_field');

        add_settings_field(PRIVATE_SITE_OPTION, __('Access restriction', 'rrze-private-site'), [$this, 'privateSiteField'], 'reading');

        if (absint(get_option(PRIVATE_SITE_OPTION))) {
            add_settings_field(PRIVATE_SITE_PASSWORD_OPTION, __('Allow Password', 'rrze-private-site'), [$this, 'allowPasswordField'], 'reading');
            add_settings_field(PRIVATE_SITE_ALLOW_IPADDR_OPTION, __('Allow IP Addresses', 'rrze-private-site'), [$this, 'allowIpaddrField'], 'reading');
            if (permissions()->simplesamlAuth()) {
                add_settings_field(PRIVATE_SITE_SSO_OPTION, __('Allow Single Sign-On', 'rrze-private-site'), [$this, 'allowSSOField'], 'reading');
                if (absint(get_option(PRIVATE_SITE_SSO_OPTION))) {
                    add_settings_field(PRIVATE_SITE_ALLOW_AFFILIATION_OPTION, '&#8212; ' . __('Person affiliation', 'rrze-private-site'), [$this, 'allowAffiliationField'], 'reading');
                    add_settings_field(PRIVATE_SITE_ALLOW_ENTITLEMENT_OPTION, '&#8212; ' . __('Person entitlement', 'rrze-private-site'), [$this, 'allowEntitlementField'], 'reading');
                }
            }

            add_settings_field(PRIVATE_SITE_FEED_TOKEN_OPTION, __('Allow Feed Token', 'rrze-private-site'), [$this, 'feedTokenField'], 'reading');
            add_settings_field(PRIVATE_SITE_CONTACT_ADMIN_NAME_OPTION, __('Contact', 'rrze-private-site'), [$this, 'contactAdminNameField'], 'reading');
        }
    }

    /**
     * validateActivation
     * @param  string $input
     * @return string
     */
    public function validateActivation($input)
    {
        return !empty($input) ? '1' : '0';
    }

    /**
     * validateFeedToken
     * @param  string $input
     * @return string
     */
    public function validatePassword($input)
    {
        $input = !empty($input) ? sanitize_text_field(trim($input)) : '';
        if ($input != '' && !preg_match('/^[a-z0-9]{8,32}$/i', $input)) {
            $input = '';
            add_settings_error(
                PRIVATE_SITE_PASSWORD_OPTION,
                PRIVATE_SITE_PASSWORD_OPTION,
                __('The password must be an alphanumeric value between 8 and 32 characters.', 'rrze-private-site'),
                'error'
            );
        }
        return $input;
    }

    /**
     * validateAllowIp
     * @param  string $input
     * @return array
     */
    public function validateAllowIp($input)
    {
        $input = !empty($input) ? sanitize_textarea_field(trim($input)) : '';
        $input = !empty($input) ? array_unique(array_map('trim', explode(PHP_EOL, $input))) : '';
        if ($input && is_null($input = $this->sanitizeIpRange($input))) {
            $input = '';
            add_settings_error(
                PRIVATE_SITE_ALLOW_IPADDR_OPTION,
                PRIVATE_SITE_ALLOW_IPADDR_OPTION,
                __('One of the entered IP addresses is invalid.', 'rrze-private-site'),
                'error'
            );
        }
        return $input;
    }

    /**
     * validateSSO
     * @param  string $input
     * @return string
     */
    public function validateSSO($input)
    {
        return !empty($input) ? '1' : '0';
    }

    /**
     * validateAllowAffiliation
     * @param  string $input
     * @return array
     */
    public function validateAllowAffiliation($input)
    {
        $input = !empty($input) ? sanitize_textarea_field(trim($input)) : '';
        return !empty($input) ? array_unique(array_map('trim', explode(PHP_EOL, $input))) : '';
    }

    /**
     * validateAllowEntitlement
     * @param  string $input
     * @return array
     */
    public function validateAllowEntitlement($input)
    {
        $input = !empty($input) ? sanitize_textarea_field(trim($input)) : '';
        return !empty($input) ? array_unique(array_map('trim', explode(PHP_EOL, $input))) : '';
    }

    /**
     * validateFeedToken
     * @param  string $input
     * @return string
     */
    public function validateFeedToken($input)
    {
        $input = !empty($input) ? sanitize_text_field(trim($input)) : '';
        if ($input != '' && !preg_match('/^[a-z0-9]{8,32}$/i', $input)) {
            $input = '';
            add_settings_error(
                PRIVATE_SITE_FEED_TOKEN_OPTION,
                PRIVATE_SITE_FEED_TOKEN_OPTION,
                __('The feed token must be an alphanumeric value between 8 and 32 characters.', 'rrze-private-site'),
                'error'
            );
        }
        return $input;
    }

    /**
     * privateSiteField
     */
    public function privateSiteField()
    {
        $active = absint(get_option(PRIVATE_SITE_OPTION)) ? 1 : 0; ?>
        <fieldset>
            <legend class="screen-reader-text">
                <span><?php _e('Access restriction', 'rrze-private-site'); ?></span>
            </legend>
            <label for="rrze-private-site">
                <input type="checkbox" id="rrze-private-site" name="<?php echo PRIVATE_SITE_OPTION; ?>" value="1" <?php checked('1', $active); ?>>
                <?php _e('Enable Access Restriction', 'rrze-private-site'); ?>
            </label>
            <?php if ($active) : ?>
                <p class="description"><?php _e('This website is accessible to registered members.', 'rrze-private-site'); ?></p>
            <?php endif; ?>
        </fieldset>
    <?php
    }

    /**
     * allowPasswordField
     */
    public function allowPasswordField()
    {
        $password = get_option(PRIVATE_SITE_PASSWORD_OPTION, ''); ?>
        <input type="text" id="rrze-private-site-password" class="regular-text" name="<?php echo PRIVATE_SITE_PASSWORD_OPTION; ?>" value="<?php echo $password; ?>">
        <p class="description"><?php _e('Allows access using a password (alphanumeric value between 8 and 32 characters).', 'rrze-private-site'); ?></p>
    <?php
    }

    /**
     * allowIpaddrField
     */
    public function allowIpaddrField()
    {
        $allowedIpAddress = get_option(PRIVATE_SITE_ALLOW_IPADDR_OPTION);
        $allowedIpAddress = !empty($allowedIpAddress) && is_array($allowedIpAddress) ? implode(PHP_EOL, $allowedIpAddress) : ''; ?>
        <textarea id="rrze-private-site-allow-ip-address" cols="50" rows="5" name="<?php echo PRIVATE_SITE_ALLOW_IPADDR_OPTION; ?>"><?php echo $allowedIpAddress; ?></textarea>
        <p class="description"><?php _e('Enter one IP address per line. CIDR notation is also supported.', 'rrze-private-site'); ?></p>
    <?php
    }

    /**
     * allowSSOField
     */
    public function allowSSOField()
    {
        $active = absint(get_option(PRIVATE_SITE_SSO_OPTION)) ? 1 : 0; ?>
        <fieldset>
            <legend class="screen-reader-text">
                <span><?php _e('Authorize access through Single Sign-On', 'rrze-private-site'); ?></span>
            </legend>
            <label for="rrze-private-site">
                <input type="checkbox" id="rrze-private-site" name="<?php echo PRIVATE_SITE_SSO_OPTION; ?>" value="1" <?php checked('1', $active); ?>>
                <?php _e('Authorize access through Single Sign-On', 'rrze-private-site'); ?>
            </label>
            <?php if ($active) : ?>
                <p class="description">
                    <?php _e('This website is accessible to IdM accounts.', 'rrze-private-site'); ?><br>
                    <?php _e('Access through Single Sign-On can be restricted by the following person attributes (if available):', 'rrze-private-site'); ?>
                </p>
            <?php endif; ?>
        </fieldset>
    <?php
    }

    /**
     * allowAffiliationField
     */
    public function allowAffiliationField()
    {
        $allowedAffiliation = get_option(PRIVATE_SITE_ALLOW_AFFILIATION_OPTION, '');
        $allowedAffiliation = is_array($allowedAffiliation) ? implode(PHP_EOL, $allowedAffiliation) : ''; ?>
        <textarea id="rrze-private-site-allow-entitlement" cols="50" rows="3" name="<?php echo PRIVATE_SITE_ALLOW_AFFILIATION_OPTION; ?>"><?php echo $allowedAffiliation; ?></textarea>
        <p class="description"><?php _e('Enter one person affiliation per line.', 'rrze-private-site'); ?></p>
    <?php
    }

    /**
     * allowEntitlementField
     */
    public function allowEntitlementField()
    {
        $allowedEntitlement = get_option(PRIVATE_SITE_ALLOW_ENTITLEMENT_OPTION, '');
        $allowedEntitlement = is_array($allowedEntitlement) ? implode(PHP_EOL, $allowedEntitlement) : ''; ?>
        <textarea id="rrze-private-site-allow-entitlement" cols="50" rows="3" name="<?php echo PRIVATE_SITE_ALLOW_ENTITLEMENT_OPTION; ?>"><?php echo $allowedEntitlement; ?></textarea>
        <p class="description"><?php _e('Enter one person entitlement per line.', 'rrze-private-site'); ?></p>
    <?php
    }

    /**
     * feedTokenField
     */
    public function feedTokenField()
    {
        $feedToken = get_option(PRIVATE_SITE_FEED_TOKEN_OPTION, '');
        $description = sprintf(
            /* translators: 1: The Site URL, 2: The Feed Token. */
            __('Usage: %1$s/feed/?token=%2$s', 'rrze-private-site'),
            site_url(),
            $feedToken
        ); ?>
        <input type="text" id="rrze-private-site-feed-token" class="regular-text" name="<?php echo PRIVATE_SITE_FEED_TOKEN_OPTION; ?>" value="<?php echo $feedToken; ?>">
        <p class="description"><?php _e('Allow access to RSS feed via a token (alphanumeric value between 8 and 32 characters).', 'rrze-private-site'); ?></p>
        <?php if ($feedToken) : ?>
            <p class="description"><?php echo $description; ?></p>
        <?php endif; ?>
    <?php
    }

    /**
     * contactAdminNameField
     */
    public function contactAdminNameField()
    {
        $contact = get_option(PRIVATE_SITE_CONTACT_ADMIN_NAME_OPTION, ''); ?>
        <input type="text" id="rrze-private-site-contact" class="regular-text" name="<?php echo PRIVATE_SITE_CONTACT_ADMIN_NAME_OPTION; ?>" value="<?php echo $contact; ?>">
        <p class="description">
            <?php printf(
                '%s: <strong><i>%s</strong><br>%s<br>%s',
                __('The name of the contact that corresponds to the website administration email address', 'rrze-private-site'),
                get_option('admin_email'),
                __('The contact will be displayed in all access denied messages.', 'rrze-private-site'),
                __('If this field is left empty, all users with the administrator role will be listed as contacts in all access denied messages.', 'rrze-private-site')
            ); ?>
        </p>
    <?php
    }

    /**
     * Sanitize An Ip Range
     * @param  array $ipAddr
     * @return null|array
     */
    protected function sanitizeIpRange(array $ipAddr)
    {
        $ipRange = [];
        foreach ($ipAddr as $key => $value) {
            $sanitizedValue = IPUtils::sanitizeIpRange($value);
            if (!is_null($sanitizedValue)) {
                $ipRange[] = $sanitizedValue;
            } else {
                // Error: The IP address is not valid!
                return null;
            }
        }
        return $ipRange;
    }

    public function pluginActionLink($links)
    {
        return array_merge(
            $links,
            [
                sprintf(
                    '<a href="%s">%s</a>',
                    admin_url('options-reading.php'),
                    __('Settings', 'rrze-private-site')
                )
            ]
        );
    }
}
