<?php

/*
Plugin Name:        RRZE Private Site
Plugin URI:         https://gitlab.rrze.fau.de/rrze-webteam/rrze-private-site
Version:            2.8.6
Description:        Restricts access to the website.
Author:             RRZE Webteam
Author URI:         https://www.rrze.fau.de
License:            GNU General Public License Version 3
License URI:        https://www.gnu.org/licenses/gpl-3.0.html
Text Domain:        rrze-private-site
Domain Path:        /languages
Requires at least:  6.7
Requires PHP:       8.2
*/

namespace RRZE\PrivateSite;

defined('ABSPATH') || exit;

const PRIVATE_SITE_OPTION = 'private_site';
const PRIVATE_SITE_PASSWORD_OPTION = 'private_site_password';
const PRIVATE_SITE_ALLOW_IPADDR_OPTION = 'private_site_allow_ipaddr';
const PRIVATE_SITE_SSO_OPTION = 'private_site_sso';
const PRIVATE_SITE_ALLOW_AFFILIATION_OPTION = 'private_site_allow_affiliation';
const PRIVATE_SITE_ALLOW_ENTITLEMENT_OPTION = 'private_site_allow_entitlement';
const PRIVATE_SITE_FEED_TOKEN_OPTION = 'private_site_feed_token';
const PRIVATE_SITE_SITEIMPROVE_OPTION = 'private_site_siteimprove';
const PRIVATE_SITE_CONTACT_ADMIN_NAME_OPTION = 'private_site_contact_admin_name';

/**
 * SPL Autoloader (PSR-4).
 * @param string $class The fully-qualified class name.
 * @return void
 */
spl_autoload_register(function ($class) {
    $prefix = __NAMESPACE__;
    $baseDir = __DIR__ . '/includes/';

    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        return;
    }

    $relativeClass = substr($class, $len);
    $file = $baseDir . str_replace('\\', '/', $relativeClass) . '.php';

    if (file_exists($file)) {
        require $file;
    }
});

// Register plugin hooks.
register_activation_hook(__FILE__, __NAMESPACE__ . '\activation');
register_deactivation_hook(__FILE__, __NAMESPACE__ . '\deactivation');

add_action('plugins_loaded', __NAMESPACE__ . '\loaded');

// Load the plugin's text domain for localization.
add_action('init', fn() => load_plugin_textdomain('rrze-private-site', false, dirname(plugin_basename(__FILE__)) . '/languages'));

/**
 * Activation callback function.
 */
function activation($networkwide)
{
    //
}

/**
 * Deactivation callback function.
 */
function deactivation()
{
    update_option('blog_public', '1');
    delete_option(PRIVATE_SITE_OPTION);
}

/**
 * Instantiate Plugin class.
 * @return object Plugin
 */
function plugin()
{
    static $instance;
    if (null === $instance) {
        $instance = new Plugin(__FILE__);
    }
    return $instance;
}

/**
 * Instantiate Permissions class.
 * @return object Permissions
 */
function permissions()
{
    static $instance;
    if (null === $instance) {
        $instance = new Permissions();
    }
    return $instance;
}

/**
 * Check system requirements for the plugin.
 *
 * This method checks if the server environment meets the minimum WordPress and PHP version requirements
 * for the plugin to function properly.
 *
 * @return string An error message string if requirements are not met, or an empty string if requirements are satisfied.
 */
function systemRequirements(): string
{
    // Get the global WordPress version.
    global $wp_version, $is_apache;

    // Get the PHP version.
    $phpVersion = phpversion();

    // Initialize an error message string.
    $error = '';

    // Check if the WordPress version is compatible with the plugin's requirement.
    if (!is_wp_version_compatible(plugin()->getRequiresWP())) {
        $error = sprintf(
            /* translators: 1: Server WordPress version number, 2: Required WordPress version number. */
            __('The server is running WordPress version %1$s. The plugin requires at least WordPress version %2$s.', 'rrze-private-site'),
            $wp_version,
            plugin()->getRequiresWP()
        );
    } elseif (!is_php_version_compatible(plugin()->getRequiresPHP())) {
        // Check if the PHP version is compatible with the plugin's requirement.
        $error = sprintf(
            /* translators: 1: Server PHP version number, 2: Required PHP version number. */
            __('The server is running PHP version %1$s. The plugin requires at least PHP version %2$s.', 'rrze-private-site'),
            $phpVersion,
            plugin()->getRequiresPHP()
        );
    } elseif (!$is_apache) {
        $error = __('The Web server software is not compatible. Please use instead the Apache Web server software.', 'rrze-private-site');
    } elseif (!apache_mod_loaded('mod_rewrite', true)) {
        $error = __('The Web server software does not support the Rewrite module.', 'rrze-private-site');
    } elseif (is_multisite() && is_plugin_active_for_network(plugin()->getBaseName())) {
        $error = __('This plugin can not be activated networkwide', 'rrze-private-site');
    } elseif (is_multisite() && get_current_blog_id() == 1) {
        $error = __('The plugin cannot be installed on the main website of a WordPress multisite network.', 'rrze-private-site');
    }

    // Return the error message string, which will be empty if requirements are satisfied.
    return $error;
}

/**
 * Handle the loading of the plugin.
 *
 * This function is responsible for initializing the plugin, loading text domains for localization,
 * checking system requirements, and displaying error notices if necessary.
 */
function loaded()
{
    // Trigger the 'loaded' method of the main plugin instance.
    plugin()->loaded();
    // Check system requirements.
    if (systemRequirements()) {
        // If there is an error, add an action to display an admin notice with the error message.
        add_action('admin_init', function () {
            $error = systemRequirements();
            // Check if the current user has the capability to activate plugins.
            if (current_user_can('activate_plugins')) {
                // Get plugin data to retrieve the plugin's name.
                $pluginName = plugin()->getName();

                // Determine the admin notice tag based on network-wide activation.
                $tag = is_plugin_active_for_network(plugin()->getBaseName()) ? 'network_admin_notices' : 'admin_notices';

                // Add an action to display the admin notice.
                add_action($tag, function () use ($pluginName, $error) {
                    printf(
                        '<div class="notice notice-error"><p>' .
                            /* translators: 1: The plugin name, 2: The error string. */
                            esc_html__('Plugins: %1$s: %2$s', 'rrze-legal') .
                            '</p></div>',
                        $pluginName,
                        $error
                    );
                });
            }
        });

        // Return to prevent further initialization if there is an error.
        return;
    }

    // If there are no errors, create an instance of the 'Main' class
    new Main;
}
